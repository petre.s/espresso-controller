
// include the library code:

#include <Arduino.h>

#include <SPI.h>
#include <Wire.h>

#define W_N   3

uint8_t brewButtonPin = A2;

uint8_t brewPin = 12;


enum State {
  idle, 
  pre_start,
  sending_start_brew_signal,
  brewing,
  sending_stop_brew_signal
};

unsigned long last_idle_weight_read_time = 0;
State mainState = idle;


uint8_t scaleDataPin = PD3;
uint8_t scaleClockPin = PD2;

uint32_t start, stop;

// flow
unsigned long lastFlowReadTime = 0;
float flowW1 = 0, flowW2 = 0;
float flow = 0;
unsigned int flowIntervalMilis = 500;
unsigned long lastValidShotFlow = 0;



boolean brewButton;
boolean brewButtonPrev;

uint8_t weightTarget = 36;
uint8_t weightTargetAddress = 0;

float currentWeight = 0.0;


float w[W_N];

unsigned long w_i = 0;

boolean brewSignalUp = false;
boolean brewSignalSent = false;
unsigned long brewSignalTime = 0;
unsigned long brewSignalDurationMs = 600;


boolean brewPressed(){
  return brewButton && (!brewButtonPrev);
}


void readButtons(){
  brewButtonPrev = brewButton;
  brewButton = digitalRead(brewButtonPin);
}

void changeModel(){
  
  unsigned long nowMillis = millis();

  if (mainState == idle && nowMillis - last_idle_weight_read_time > 200 ) {
    measure_while_idle();
  }

  if (mainState == idle && brewPressed()) {
    mainState = pre_start;
  }

  if (mainState == pre_start && idle_weight_is_stable()) {
    mainState = brewing;
  }
  
  
  if (mainState == sending_stop_brew_signal && (nowMillis > brewSignalTime + brewSignalDurationMs)) {
    brewSignalUp = false;
    mainState = idle;
  } 
  
  if ((mainState == brewing) && (currentWeight + 0.7*flow >= weightTarget)){
    mainState = sending_stop_brew_signal;
    brewSignalUp = true;
    brewSignalTime = millis();
  }
}

void updateOutput(){

  
  if (brewSignalUp){
    digitalWrite(brewPin, HIGH);
    Serial.print("HIGH");
  } else {
    digitalWrite(brewPin, LOW);
  }
}




// to be called after read scale
void processFlow() {
    unsigned long nowMillis =  millis();
    
    if (nowMillis - lastFlowReadTime < flowIntervalMilis){
      return;
    }
    
    
    flowW1 = flowW2;
    flowW2 = currentWeight;
 
 
    if (lastFlowReadTime != 0){
      flow = roundZero( (flowW2 -flowW1) / ((nowMillis- lastFlowReadTime) / 1000.0));  
      Serial.println(flow);
    } 
    
    lastFlowReadTime = nowMillis;

    // calculate / invalidate last lastValidShotFlow
    if (flow < 0.5 || flow > 5) {
        lastValidShotFlow = 0;
    } else if (lastValidShotFlow == 0) {
      lastValidShotFlow = nowMillis;
    }
}


void measure_while_idle(){
    w[w_i % W_N] = 25; // read scale here
    w_i++;
    last_idle_weight_read_time = millis();
}

boolean idle_weight_is_stable(){
    if (w_i < W_N){
      return false;
    }
    
    for (uint8_t i= 1; i< W_N; i++){
      if (abs (w[0] - w[i]) > 0.2) {
        return false;
      }
    }
    return true;
}


void measure(){
  
    w[w_i % W_N] = 25; // read scale here
    w_i++;
    
    float sum = 0;
    for (uint8_t i= 0; i< min(W_N, w_i); i++){
      sum += w[i]; 
    }

    currentWeight = roundZero(sum / min(W_N, w_i));
}

float roundZero(float x){
   if (x >-0.1 && x < 0.1){
      return 0.0;
    } else {
      return x;
    }
}


void setup() {
  Serial.print("  SETUP: ");
  Serial.begin(9600);

  pinMode(brewButtonPin, INPUT);
  
  pinMode(brewPin, OUTPUT);
  

  weightTarget = 40;

   for (uint8_t i= 0; i< W_N; i++){
      w[i] = 0; 
    }

}

void loop() {
//  measure();
  processFlow(); 
  readButtons();
  changeModel();
  updateOutput();


 
  
}
